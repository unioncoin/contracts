pragma solidity ^0.5.8;

import "./ERC20Token.sol";

contract UnionCoin is ERC20Token {
  string public constant name = "UnionCoin";
  string public constant symbol = "UC";
  uint8 public constant decimals = 18;

  mapping (address => uint) public balances;
  mapping (address => mapping (address => uint)) allowed;

  address public burnAddress;

  constructor(address initialOwner) public {
    burnAddress = address(0x0);
    balances[initialOwner] = totalSupply();
  }

  function totalSupply() public view returns (uint) {
    return 720000000 - balances[burnAddress];
  }

  function burn(uint tokens) public returns (bool success) {
    if (balances[msg.sender] < tokens) return false;
    return transfer(burnAddress, tokens);
  }

  function balanceOf(address tokenOwner) public view returns (uint balance) {
    return balances[tokenOwner];
  }

  function allowance(address tokenOwner, address spender) public view returns (uint remaining) {
    return allowed[tokenOwner][spender];
  }

  function transfer(address to, uint tokens) public returns (bool success) {
    if (balances[msg.sender] < tokens) return false;
    balances[msg.sender] -= tokens;
    balances[to] += tokens;
    emit Transfer(msg.sender, to, tokens);
    return true;
  }

  function transferFrom(address from, address to, uint tokens) public returns (bool success) {
    if (balances[from] < tokens) return false;
    if (allowed[from][msg.sender] < tokens) return false;
    balances[from] -= tokens;
    allowed[from][msg.sender] -= tokens;
    balances[to] += tokens;
    emit Transfer(from, to, tokens);
    return true;
  }

  function approve(address spender, uint tokens) public returns (bool success) {
    allowed[msg.sender][spender] += tokens;
    emit Approval(msg.sender, spender, tokens);
    return true;
  }

}
