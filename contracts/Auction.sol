pragma solidity ^0.5.8;

import "./UnionCoin.sol";

contract Auction {
  address public tokenAddress;
  uint public startTimestamp;

  // 6 hours
  uint public cycleLengthSeconds = 21600;
  uint public tokensPerCycle = 500000;
  uint public totalCycleCount = 1440;
  mapping (uint => uint) public tokensSoldPerCycle;

  /// Purchase some tokens at the current price, if possible
  function () external payable {
    require(currentCycle() < totalCycleCount);
    uint remainingTokens = tokensPerCycle - tokensSoldPerCycle[currentCycle()];
    uint purchasedTokens = msg.value / currentTokenWeiPrice();
    uint deliveredTokens = min(remainingTokens, purchasedTokens);
    require(deliveredTokens > 0);
    uint refundWei = msg.value - (deliveredTokens * currentTokenWeiPrice());
    tokensSoldPerCycle[currentCycle()] += deliveredTokens;
    UnionCoin tokenContract = UnionCoin(tokenAddress);
    require(tokenContract.transfer(msg.sender, deliveredTokens));
    if (refundWei > 0) {
      require(msg.sender.send(refundWei));
    }
  }

  /// Initial setter for bootstrapping auction
  function setTokenAddress(address _tokenAddress) public {
    require(tokenAddress == address(0x0));
    UnionCoin tokenContract = UnionCoin(_tokenAddress);
    require(tokenContract.balanceOf(address(this)) == tokensPerCycle * totalCycleCount);
    tokenAddress = _tokenAddress;
    startTimestamp = block.timestamp;
  }

  /// Current cycle number
  function currentCycle() public view returns (uint cycleNumber) {
    return (block.timestamp - startTimestamp) / cycleLengthSeconds;
  }

  /// Current sell price, uses an algebraic sigmoid for price decay over time
  function currentTokenWeiPrice() public view returns (uint price) {
    uint cycleSecondsRemaining = cycleLengthSeconds - (block.timestamp - currentCycle() * cycleLengthSeconds - startTimestamp);
    // 100 ether total
    uint yMax = 200000000000000;
    // 1 ether total
    uint yMin = 2000000000000;

    uint xMax = cycleLengthSeconds;
    uint yRange = yMax - yMin;
    uint k = 4000000;
    uint x = cycleSecondsRemaining;

    uint b = 4 * sqrt(k + x * x + (xMax * xMax) / 4 - x * xMax);
    uint s = yRange / 2 + (2 * x * yRange) / b - (xMax * yRange) / b;

    return yMin + s;
  }

  /// Called after auction ends to remove unsold tokens from total supply
  function finalBurn() public {
    require(currentCycle() >= totalCycleCount);
    UnionCoin tokenContract = UnionCoin(tokenAddress);
    require(tokenContract.balanceOf(address(this)) > 0);
    require(tokenContract.burn(tokenContract.balanceOf(address(this))));
  }

  function sqrt(uint x) private pure returns (uint y) {
    uint z = (x + 1) / 2;
    y = x;
    while (z < y) {
      y = z;
      z = (x / z + z) / 2;
    }
  }

  function min(uint a, uint b) private pure returns (uint) {
    return a < b ? a : b;
  }

  /// Getter for external access
  function _tokensSoldPerCycle(uint cycleNumber) public view returns (uint) {
    return tokensSoldPerCycle[cycleNumber];
  }
}
