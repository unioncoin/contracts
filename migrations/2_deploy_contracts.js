const Auction = artifacts.require('Auction');
const UnionCoin = artifacts.require('UnionCoin');

/**
 * async/await and Promise.* functions do not work for the promises below
 * https://github.com/trufflesuite/truffle/issues/501#issuecomment-332589663
 **/
module.exports = function(deployer, _, accounts) {
  deployer.deploy(Auction)
    .then(() => deployer.deploy(UnionCoin, Auction.address))
    .then((_auctionContract) => {
      const auctionContract = new web3.eth.Contract(Auction.abi, Auction.address)
      return auctionContract.methods.setTokenAddress(UnionCoin.address).send({
        from: accounts[0],
        gas: 200000,
      })
    })
};
